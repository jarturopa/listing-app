import 'dotenv/config'

import { connectDatabase } from '../src/database';
import { Listing, User } from '../src/lib/types';
import { users as UsersSeed, listings as ListingsSeed } from './seeds';

const listings: Listing[] = ListingsSeed;
const users: User[] = UsersSeed;

const seed = async () => {
  try {
    console.log('[seed] : running...');

    const db = await connectDatabase();

    for (const listing of listings) {
      await db.listings.insertOne(listing);
    }

    for (const user of users) {
      await db.users.insertOne(user);
    }

    console.log('[seed] : success');
    return;
  } catch {
    throw new Error('Failed to seed database');
  }
}

seed();