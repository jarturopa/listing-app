import { MongoClient } from 'mongodb';
import { Database, Booking, Listing, User } from '../lib/types';

// const options = 'ssl=true&replicaSet=atlas-lvuita-shard-0&authSource=admin&retryWrites=true&w=majority';
const options = 'retryWrites=true&w=majority&appName=Cluster0';

const url = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_USER_PASSWORD}@${process.env.DB_CLUSTER}${options}`;
const mongoClient = new MongoClient(url);

export const connectDatabase = async (): Promise<Database> => {
  const client = await mongoClient.connect();
  const db = client.db('main');

  return {
    bookings: db.collection<Booking>('bookings'),
    listings: db.collection<Listing>('listings'),
    users: db.collection<User>('users'),
  };
};
