import 'dotenv/config'
import express, { Application } from 'express';
import { ApolloServer } from 'apollo-server-express';
import {
  ApolloServerPluginDrainHttpServer,
  ApolloServerPluginLandingPageLocalDefault
} from 'apollo-server-core';
import http from 'http';

import { connectDatabase } from './database';
import { typeDefs, resolvers } from './graphql';

async function startApolloServer(app: Application) {
  const db = await connectDatabase();
  const httpServer = http.createServer(app);
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: () => ({ db }),
    csrfPrevention: true,
    cache: 'bounded',
    plugins: [
      ApolloServerPluginDrainHttpServer({ httpServer }),
      ApolloServerPluginLandingPageLocalDefault({ embed: true })
    ]
  });

  await server.start();
  server.applyMiddleware({ app, path: '/api' });
  await new Promise<void>(resolve => httpServer.listen({ port: process.env.PORT }, resolve))

  console.log(`[app]: http://localhost:${process.env.PORT}`);
}

startApolloServer(express());
